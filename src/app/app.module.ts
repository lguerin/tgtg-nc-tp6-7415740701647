import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoresComponent } from './components/stores/stores.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { StoreCardsComponent } from './components/store-cards/store-cards.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { FilterComponent } from './components/stores/filter/filter.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ExpiredSincePipe } from './pipes/expired-since.pipe';
import { StoreCardComponent } from './components/store-cards/store-card/store-card.component';

@NgModule({
  declarations: [
    AppComponent,
    StoresComponent,
    StoreCardComponent,
    StoreCardsComponent,
    FilterComponent,
    ExpiredSincePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
